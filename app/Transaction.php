<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'tran_id',
        'cus_name',
        'cus_email',
        'cus_phone',
        'total_amount',
        'store_amount',
        'currency',
        'cus_add1',
        'cus_add2',
        'cus_city',
        'cus_postcode',
        'cus_country',
        'status',
        'failedreason',
        'error',
        'sessionkey',
        'verify_key',
        'key',
        'verify_sign',
        'verify_sign_sha2',
        'val_id',
        'bank_tran_id',
        'card_type',
        'card_no',
        'card_issuer',
        'card_brand',
        'card_issuer_country',
        'card_issuer_country_code',
        'emi_option',
        'risk_level',
        'risk_title',
        'tran_date',
    ];
}
