<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\TransactionController;

use App\Transaction;

class ResponseController extends Controller
{

    public function success(Request $request)
    {
        $tran_id = $request->tran_id;

        $params = [
            'val_id' => $request->val_id,
            'store_amount' => $request->store_amount,
            'risk_level' => $request->risk_level,
            'risk_level' => $request->risk_level,
            'risk_title' => $request->risk_title,

            'apn_status' => $request->status,
            'apn_error' => $request->error,
            'bank_tran_id' => $request->bank_tran_id,
            'tran_date' => $request->tran_date,

            'verify_sign' => $request->verify_sign,
            'verify_sign_sha2' => $request->verify_sign_sha2,
            'verify_key' => $request->verify_key,
            'key' => $request->key,

            'card_type' => $request->card_type,
            'card_no' => $request->card_no,
            'card_issuer' => $request->card_issuer,
            'card_brand' => $request->card_brand,
            'card_issuer_country' => $request->card_issuer_country,
            'card_issuer_country_code' => $request->card_issuer_country_code,
        ];

        TransactionController::validateApi($request, $request->val_id);

        Transaction::where('tran_id', $tran_id)->update($params);

        return redirect('/transaction');;
    }

    public function fail(Request $request)
    {
        $tran_id = $request->tran_id;

        $params = [
            'apn_status' => $request->status,
            'apn_error' => $request->error,
            'bank_tran_id' => $request->bank_tran_id,
            'tran_date' => $request->tran_date,

            'verify_sign' => $request->verify_sign,
            'verify_sign_sha2' => $request->verify_sign_sha2,
            'verify_key' => $request->verify_key,
            'key' => $request->key,

            'card_type' => $request->card_type,
            'card_no' => $request->card_no,
            'card_issuer' => $request->card_issuer,
            'card_brand' => $request->card_brand,
            'card_issuer_country' => $request->card_issuer_country,
            'card_issuer_country_code' => $request->card_issuer_country_code,
        ];

        Transaction::where('tran_id', $tran_id)->update($params);

        return redirect('/transaction');;
    }

    public function cancel(Request $request)
    {
        $tran_id = $request->tran_id;

        $params = [
            'apn_status' => $request->status,
            'apn_error' => $request->error,
            'bank_tran_id' => $request->bank_tran_id,
            'tran_date' => $request->tran_date,
            'verify_sign' => $request->verify_sign,
            'verify_sign_sha2' => $request->verify_sign_sha2,
            'verify_key' => $request->verify_key,
        ];

        Transaction::where('tran_id', $tran_id)->update($params);

        return redirect('/transaction');;
    }
}
