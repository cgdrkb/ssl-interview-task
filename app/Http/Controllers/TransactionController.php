<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

use App\Transaction;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all();
        return view('transactions.list')->with(compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Validate the transaction using validation API.
     *
     * @param  int  $val_id
     * @return \Illuminate\Http\Response
     */
    public static function validateApi(Request $request, $val_id)
    {
        $params = [
            'format' => 'json',
            'val_id' => $val_id,
            'store_id' => config('app.sslcommerz.store_id'),
            'store_passwd' => config('app.sslcommerz.store_passwd'),
        ];

        // $client = new Client(['base_uri' => 'https://sandbox.sslcommerz.com/validator/api/']);
        // // $response = $client->request('GET', 'validationserverAPI.php', ['form_params' => $params]);
        // $response = $client->get('validationserverAPI.php', ['form_params' => $params]);
        // $responseBodyContents = $response->getBody()->getContents();
        $responseBodyContents = file_get_contents( 'https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?format=json&val_id='.$params['val_id'].'&store_id=testbox&store_passwd=qwerty');
        $data = json_decode($responseBodyContents);

        $transaction = Transaction::where('tran_id', $data->tran_id)->first();
        $transaction->validation_status =  $data->status;
        $transaction->save();

        return redirect('/transaction');
    }
}
