<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Str;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

use App\Transaction;

class CheckoutController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->show(0);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = [

            'store_id' => config('app.sslcommerz.store_id'),
            'store_passwd' => config('app.sslcommerz.store_passwd'),
            'success_url' => config('app.sslcommerz.success_url'),
            'fail_url' => config('app.sslcommerz.fail_url'),
            'cancel_url' => config('app.sslcommerz.cancel_url'),
            'emi_option' => config('app.sslcommerz.emi_option'),
            'total_amount' => $request->total_amount,
            'product_amount' => $request->total_amount,
            'currency' =>  $request->currency,
            'tran_id' =>  Str::random(16),
            'cus_name' =>  $request->cus_name,
            'cus_email' =>  $request->cus_email,
            'cus_phone' =>  $request->cus_phone,
            'cus_add1' => $request->cus_add1,
            'cus_add2' => $request->cus_add2,
            'cus_city' => $request->cus_city,
            'cus_postcode' => $request->cus_postcode,
            'cus_country' => $request->cus_country,

        ];

        $products = [ // Demo purpose only. These data will come from database in a real system.
            0 => ['product' => 'Unknown', 'amount' => 0.00],
            1 => ['product' => 'Running Shoe', 'amount' => 799.99],
            2 => ['product' => 'Men Luxury Watch', 'amount' => 349.99],
            3 => ['product' => 'Mobile Phone', 'amount' => 499.99],
        ];
        $params['cart'] = json_encode($products[$request->product_id]);
// dd($params);
        $client = new Client(['base_uri' => 'https://sandbox.sslcommerz.com/gwprocess/v3/']);
        $response = $client->request('POST', 'api.php', ['form_params' => $params]);
        $responseBodyContents = $response->getBody()->getContents();
        $data = json_decode($responseBodyContents);
        $params['conn_status'] = $data->status;
        $params['conn_failedreason'] =  $data->failedreason;
        $params['sessionkey'] =  $data->sessionkey;

        $transaction = new Transaction($params);
        $transaction->save();

        return redirect($data->redirectGatewayURL);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('checkout.new')->with('user', Auth::user())->with('product_id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
