<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tran_id', 30)->nullable();

            $table->string('cus_name', 50)->nullable();
            $table->string('cus_email', 50)->nullable();
            $table->string('cus_phone', 20)->nullable();
            $table->float('total_amount', 10, 2)->nullable();
            $table->float('store_amount', 10, 2)->nullable();
            $table->string('currency', 3)->nullable();

            $table->string('cus_add1', 50)->nullable();
            $table->string('cus_add2', 50)->nullable();
            $table->string('cus_city', 50)->nullable();
            $table->string('cus_postcode', 30)->nullable();
            $table->string('cus_country', 50)->nullable();

            $table->string('conn_status', 10)->nullable();
            $table->string('conn_failedreason', 255)->nullable();
            $table->string('apn_status', 10)->nullable();
            $table->string('apn_error', 255)->nullable();
            $table->string('validation_status', 10)->nullable();
            $table->string('sessionkey', 50)->nullable();

            $table->string('verify_key')->nullable();
            $table->string('key')->nullable();
            $table->string('verify_sign', 255)->nullable();
            $table->string('verify_sign_sha2', 255)->nullable();
            $table->string('val_id', 50)->nullable();

            $table->string('bank_tran_id', 50)->nullable();
            $table->string('card_type', 50)->nullable();
            $table->string('card_no', 30)->nullable();
            $table->string('card_issuer', 50)->nullable();
            $table->string('card_brand', 30)->nullable();
            $table->string('card_issuer_country', 50)->nullable();
            $table->string('card_issuer_country_code', 2)->nullable();
            $table->tinyInteger('emi_option', false, true)->nullable();
            $table->tinyInteger('risk_level', false, true)->nullable();
            $table->string('risk_title', 50)->nullable();

            $table->dateTime('tran_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
