## Problem set

Please create a form and pass the parameters to SSLCommerz payment gateway. Fields are given below.

Customer Name(Mandatory)
Customer Email(Mandatory)
Customer Phone(Mandatory)
Amount(Mandatory)
Currency(Mandatory)
Address

Also use Google login for authentication

Store Id: testbox
Password: qwerty

For more information please check https://developer.sslcommerz.com/docs.html

Full task should be in laravel.


## Limitations
This app is for fulfilling the minimum requirements  with simplicity.

    * No shopping cart, just some demo products.
    * Existing users can use google sign-in, but google users are not allowed to set passwords.
    * Seperate tables for customer, order is not used.
    * Very basic Access Control. No admin and user separation.

## How to install

### Zip file
    * Download the zip file
    * unzip it
    * run ```php artisan serve```

### Git repo
1. clone the repo
```
git clone https://cgdrkb@bitbucket.org/cgdrkb/ssl-interview-task.git
```
2. install dependencies ``` composer update ```

3. make .env file
```
APP_DEBUG=false

DB_CONNECTION=sqlite
DB_DATABASE=/home/rkb/Code/ssl-interview-task/database/database.sqlite
DB_FOREIGN_KEYS=true

SSLCOMMERZ_STORE_ID=testbox
SSLCOMMERZ_STORE_PASSWD=qwerty
SSLCOMMERZ_SUCCESS_URL=http://127.0.0.1:8000/response/success
SSLCOMMERZ_FAIL_URL=http://127.0.0.1:8000/response/fail
SSLCOMMERZ_CANCEL_URL=http://127.0.0.1:8000/response/cancel
SSLCOMMERZ_EMI_OPTION=0 # 1 = enabled, 0 = disabled

GOOGLE_CLIENT_ID=1073507415251-nihve311hg5r23bgfjbeer3kdqapa558.apps.googleusercontent.com
GOOGLE_CLIENT_SECRET=xZ-6i4Fp5SagUQAGK13bE85-
GOOGLE_REDIRECT= http://127.0.0.1:8000/login/google/callback

```

4. Fix the database path.
5. run ```php artisan serve```

### Server URL
For convenience, app is also uploaded temporarily to a server.
1. Go to https://104.168.203.63:8000/
