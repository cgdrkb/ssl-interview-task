<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('checkout', 'CheckoutController');
Route::post('response/success', 'ResponseController@success');
Route::post('response/fail', 'ResponseController@fail');
Route::post('response/cancel', 'ResponseController@cancel');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', function () {
    return redirect('/');
});

Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('transaction', 'TransactionController@index');
Route::get('transaction/{val_id}/validate', 'TransactionController@validateApi');
