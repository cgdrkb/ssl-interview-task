@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-10">

        @if ($transactions)
            <table class="table table-striped table-hover">
                <tr class="bg-dark text-light">
                    <th>Sl.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Amount</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                <?php $sl = 0; ?>
                @foreach ($transactions as $item)
                    <tr>
                        <td>{{++$sl}}</td>
                        <td>{{ $item->cus_name }}</td>
                        <td>{{ $item->cus_email }}</td>
                        <td>{{ $item->cus_phone }}</td>
                        <td>{{ $item->total_amount }}</td>
                        <td>{{ $item->cus_add1 . ', ' . $item->cus_add2 . ', '. $item->cus_city.' - '.$item->cus_postcode.', '.$item->cus_country }}</td>
                        <td>{{ $item->validation_status ? $item->validation_status : $item->apn_status }}</td>
                        <td>@if ($item->val_id)
                                <a href="/transaction/{{ $item->val_id }}/validate" class="btn btn-link">Validate Again</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        @endif


    </div>
</div>
@endsection
