@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header bg-dark text-light font-weight-bold">Checkout with SSLCommerz</div>

            <div class="card-body">

                <form action="/checkout" method="POST" class="col-md-offset-2">
                    @csrf

                    <div class="form-group">
                        <label for="cus_name">Customer Name</label>
                    <input type="text" name="cus_name" id="cus_name" value="{{ $user->name }}" class="form-control" placeholder="Example: Jon Doe" required>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="cus_email">Customer Email</label>
                            <input type="email" name="cus_email" id="cus_email" value = "{{ $user->email }}" class="form-control" placeholder="Example: jon.doe@sslwireless.com" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="cus_phone">Customer Phone</label>

                            <input type="text" name="cus_phone" id="cus_phone" class="form-control" placeholder="Example: +8801913000006" required>
                        </div>
                    </div>

                    <div class="form-row">
                        @if ($product_id > 0)

                        <div class="form-group col-md-4">
                            <label for="product_id">Product</label>
                            <select name="product_id" id="product_id" class="form-control" >
                                <option value="0" {{ $product_id == 0 ? "selected" : ""}}>Select One</option>
                                <option value="1" {{ $product_id == 1 ? "selected" : ""}}>Running Shoe</option>
                                <option value="2" {{ $product_id == 2 ? "selected" : ""}}>Men Luxury Watch</option>
                                <option value="3" {{ $product_id == 3 ? "selected" : ""}}>Mobile Phone</option>
                            </select>
                        </div>

                        @else

                            <input type="hidden" name="product_id" value="{{ $product_id }}">

                        @endif

                        <div class="form-group col-md-4">
                            <label for="total_amount">Total Amount</label>
                            <input type="number" name="total_amount" id="total_amount"
                                    value="<?php switch($product_id) {
                                            case(1):
                                                echo "799.99";
                                                break;
                                            case(2):
                                                echo "349.99";
                                                break;
                                            case(3):
                                                echo "499.99";
                                                break;
                                            default:
                                                break;
                                            }
                                        ?>"
                                class="form-control" placeholder="Total Amount" min="10" step="0.01" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="currency">Currency Name</label>
                            <select name="currency" id="currency" class="form-control" >
                                <option value="BDT">Bangladeshi Taka (BDT)</option>
                                <option value="USD">US Dollar (USD)</option>
                                <option value="EUR">Euro (EUR)</option>
                                <option value="INR">Indian Rupee (INR)</option>
                            </select>
                        </div>
                    </div>

                    <label for="cus_add1">Customer Address</label>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="cus_add1" id="cus_add1" class="form-control" placeholder="Address Line 1">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" name="cus_add2" id="cus_add2" class="form-control" placeholder="Address Line 2">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" name="cus_city" id="cus_city" class="form-control" placeholder="City">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="number" name="cus_postcode" id="cus_postcode" class="form-control" placeholder="Postcode">
                        </div>
                        <div class="form-group col-md-4">
                            <input type="text" name="cus_country" id="cus_country" class="form-control" placeholder="Country">
                        </div>
                    </div>

                    <input type="submit" value="Checkout" class="btn btn-lg btn-dark float-right">

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
