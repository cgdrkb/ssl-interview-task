@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card" style="">
                <img src="{{url('img/prod1.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">Running Shoe</h5>
                    <p class="card-text">These shoes are tested by The Incredibles, and quality tested by The Flash. You will be able to run like them if you buy this product only from us. It will transform you into a superhero.</p>
                    <span class="card-link lead text-info font-weight-bold">৳799.99</span>
                <a href="{{url('checkout/1')}}" class="btn btn-dark float-right">Buy Shoe</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card" style="">
                <img src="{{url('img/prod2.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">Men Luxury Watch</h5>
                    <p class="card-text">This is not only a watch. This is revolution. James Bond uses this particular model. If you buy this from us, you will be the next james bond. Everybody will envy you. So, go ahead, and click buy.</p>
                    <span class="card-link lead text-info font-weight-bold">৳349.99</span>
                    <a href="{{url('checkout/2')}}" class="btn btn-dark float-right">Buy Watch</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="">
                <img src="{{url('img/prod3.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">Mobile Phone</h5>
                    <p class="card-text">A very good mobile phone. Your life will be changed if you buy this smart device. You will become so smart that even you would not believe yourself. Please buy this from us.</p>
                    <span class="card-link lead text-info font-weight-bold">৳499.99</span>
                    <a href="{{url('checkout/3')}}" class="btn btn-dark float-right">Buy Phone</a>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
